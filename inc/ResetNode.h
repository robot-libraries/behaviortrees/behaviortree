#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//
//======================================================================================================================
//    A Reset decorator calls reset on its child. Used for the few nodes that have memory: Succeed, Star composites, etc
//
//------------------------------------------------------------------------------------------------------------------------
#include <DecoratorNode.h>
namespace BT {

    class ResetNode : public DecoratorNode {
    public:
        ResetNode(TreeNode& child, std::string const& name = "reset") :
            DecoratorNode { name, child, __FUNCTION__ } {
        }

        virtual ~ResetNode() = default;

        virtual NodeState const eval() override {
            Depth depth;
            mys::tout << depth << getNodeText();

            mChild.reset();
            return mChild.getState();
        }
    };
}
