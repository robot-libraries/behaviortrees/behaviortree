#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//
//======================================================================================================================
//  A composite node has more than one child
//
//  This is an abstract class for nodes that require more than a single child
//--------------------------------------------------------------------------------------------------------------------------
#include "ControlNode.h"
//--------------------------------------------------------------------------------------------------------------------------
namespace BT {
    using TreeNodeRef = std::reference_wrapper<TreeNode>;
    using TreeVect = std::vector<TreeNodeRef>;
    //--------------------------------------------------------------------------------------------------------------------------
    class CompositeNode : public ControlNode {
    public:

        CompositeNode(std::string const& name, TreeVect v, std::string const& node_name = __FUNCTION__) :
            ControlNode(name, node_name), mChildren(v) {
        }
        virtual ~CompositeNode() = default;

    protected:
        virtual void reset() {
            for (auto child : mChildren) {
                child.get().reset();
            }
            TreeNode::reset();
        }

        TreeVect mChildren;
    };
}
