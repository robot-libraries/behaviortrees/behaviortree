#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//======================================================================================================================
//  ActionNode is an abstract node for creation of concrete action nodes by users. An ActionNode has no children.
//
//  An ActionNode should accept input and generate output values to the blackboard
//--------------------------------------------------------------------------------------------------------------------------
#include "TreeNode.h"
//--------------------------------------------------------------------------------------------------------------------------
namespace BT {
    //--------------------------------------------------------------------------------------------------------------------------
    class ActionNode : public TreeNode {
    public:

        inline ActionNode(std::string const& name, std::string const& node_name) :
            TreeNode(name, Action, node_name) {
        }
        virtual ~ActionNode() = default;
    };
//--------------------------------------------------------------------------------------------------------------------------

}
