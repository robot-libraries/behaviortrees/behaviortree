#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//
//======================================================================================================================
//
//  A Fail decorator calls its child but always returns Failure.
//
//------------------------------------------------------------------------------------------------------------------------
#include <DecoratorNode.h>
//--------------------------------------------------------------------------------------------------------------------------
namespace BT {

    class FailNode : public DecoratorNode {
    public:
        FailNode(TreeNode& child, std::string const& name = std::string { "fail" }) :
            DecoratorNode { name, child, __FUNCTION__ } {
        }
        virtual ~FailNode() = default;

        virtual NodeState const eval() override {
            Depth depth;
            mys::tout << depth << getNodeText();

            mChild.eval();
            setState(Failure);

            return getState();
        }
    };
}
