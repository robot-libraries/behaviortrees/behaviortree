#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//
//======================================================================================================================//--------------------------------------------------------------------------------------------------------------------------
//    A memory selector runs until a child returns success and then returns success. It returns failure if all children return failure.
//    A running response is returned as running.
//
//    This node remembers if nodes to the left return success and does not call them again. A call from Reset will clear the
//    memeory successful nodes
//
//    It is an OR operation on the results of the children.
//------------------------------------------------------------------------------------------------------------------------
#include "CompositeNode.h"
//--------------------------------------------------------------------------------------------------------------------------
namespace BT {

    class MemSelNode : public CompositeNode {
    public:

        MemSelNode(std::string const& name, TreeVect v) :
            CompositeNode(name, v, __FUNCTION__), mNextChild { mChildren.begin() } {
        }
        virtual ~MemSelNode() = default;

        virtual NodeState const eval() override {
            Depth depth;
            mys::tout << depth << getNodeText() << mys::tab << mChildren.size();

            setState(Success);

            mNextChild = std::find_if(mNextChild, mChildren.end(), //
                                      [&](auto& tr) {
                                          bool res { false };

                                          switch (tr.get().eval()) {
                                              case Failure:
                                                  setState(Failure);
                                                  break;

                                              case Success:
                                                  res = true;
                                                  mNextChild++;
                                                  break;

                                              case Running:
                                                  setState(Running);
                                                  res = true;
                                                  break;
                                          }
                                          return res;
                                      }
            );

            return getState();
        }

    protected:
        TreeVect::iterator mNextChild;
    };

}
