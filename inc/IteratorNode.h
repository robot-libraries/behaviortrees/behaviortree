#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//======================================================================================================================//--------------------------------------------------------------------------------------------------------------------------
//      An Iterator runs all the children returning running if a child return running. Once all children are exercised
//      it returns success regardless of success/failure of children.//
//--------------------------------------------------------------------------------------------------------------------------
namespace BT {

    class IteratorNode : public CompositeNode {
    public:

        IteratorNode(std::string const& name, TreeVect v) :
            CompositeNode(name, v, __FUNCTION__) {
        }
        virtual ~IteratorNode() = default;

        virtual NodeState const eval() override {
            Depth depth;
            mys::tout << depth << getNodeText() << mys::tab << mChildren.size();

            setState (Success);

            std::for_each(mChildren.begin(), mChildren.end(), //
                          [&](auto& tr) {
                              if (Running == tr.get().eval()) {
                                  setState (Running);
                              }
                          } //
            );

            return getState();
        }
    };
}
