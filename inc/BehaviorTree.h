#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//
//======================================================================================================================
//  Behavior Tree:
//
//  A behavior tree (BT) is a representation of the activity in an entity, which may be a robot or character in a game.
//  It provides a form of artificial intelligence.
//
//  The BT is a tree of nodes. At the tips, or leaves, are two types of nodes that do the actual decision and actions
//  of the robot.
//
//  Within the BT are control nodes that takes a single node or an aggregation of other nodes as its
//  children. A control node operates on each of its children. A control node may be a child of another control node.
//  Decision and action nodes do not have children.
//
//  Each node returns a status to the parent which may be success, failure, or running. Success means the node
//  completed its operation. Failure means the node could not compete its operation. Running means the node is not
//  finished with its operation.
//

#include <algorithm>
#include <atomic>
#include <functional>
#include <iostream>
#include <string>
#include <unistd.h>
#include <vector>

#include <Trace.h>

#include <TreeNode.h>
#include <ControlNode.h>
#include <CompositeNode.h>
#include "DecoratorNode.h"

#include "Exceptions.h"
#include "ActionNode.h"
#include "ConditionNode.h"
#include <FailNode.h>

#include <InverterNode.h>
#include <IteratorNode.h>
#include "MemSelNode.h"
#include "MemSeqNode.h"
#include <ResetNode.h>
#include <SelectorNode.h>
#include <SequenceNode.h>
#include <SucceedAlwaysNode.h>
#include <SucceedOnceNode.h>

