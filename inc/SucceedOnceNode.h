#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//
//======================================================================================================================
//      A SucceedOnce decorator reports success for its child once the child succeeds. It is useful when a series of tasks
//      must be performed without repeating completed tasks.
//
//      If a reset is received the child will be evaluated again
//------------------------------------------------------------------------------------------------------------------------
#include <DecoratorNode.h>

namespace BT {

    class SucceedOnceNode : public DecoratorNode {
    public:
        SucceedOnceNode(TreeNode& child, std::string const& name = "SucceedOnceNode") :

            DecoratorNode { name, child, __FUNCTION__ } {
        }
        virtual ~SucceedOnceNode() = default;

        virtual NodeState const eval() override {
            Depth depth;
            mys::tout << depth << getNodeText();

            mChild.eval();
            if (mSucceeded) {
                setState(Success);
            }
            else {
                if (mChild.getState() == Success) {
                    mSucceeded = true;
                    setState(Success);
                }
                else {
                    setState(Failure);
                }
            }

            return getState();
        }

        virtual void reset() override;

    protected:
        bool mSucceeded { false };

    private:

    };
    //--------------------------------------------------------------------------------------------------------------------------
    inline void SucceedOnceNode::reset() {
        mSucceeded = false;
        mChild.reset();
    }
}
