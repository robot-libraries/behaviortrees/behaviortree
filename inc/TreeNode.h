#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//
//======================================================================================================================
#include <atomic>
#include <typeinfo>

#include <Trace.h>

class Blackboard;

namespace BT {

    class TreeNode {
    public:
        enum NodeType {
            Action, Condition, Control
        };
        enum NodeState {
            Failure, Success, Running
        };

        TreeNode(std::string const& name, NodeType const type, std::string const& node_name = __FUNCTION__);
        virtual ~TreeNode() = default;

        virtual NodeState const eval() = 0;     // The method that is going to be executed by the node

        std::string const& getName() const;
        std::string getNodeId() const;
        virtual NodeState getState() const;
        NodeType const getType() const;

        virtual void reset();

        // utility methods for examining node state as text
        std::string const getNodeText() const;
        static std::string const& getStateText(NodeState const ns);
        static std::string const& getTypeText(NodeType const nt);

        struct Depth {
            Depth() {
                ++mDepth;
            }
            ~Depth() {
                --mDepth;
            }

            inline static int8_t mDepth { -1 };
        };

    protected:
        void setState(NodeState const new_state);

        std::string const mName;
        NodeType const mType;
        std::string const mNodeName;
        mutable std::atomic<NodeState> mState { Failure };

        static Blackboard mBlackboard;
    };

//--------------------------------------------------------------------------------------------------------------------------
//    inline auto indent(mys::Trace& t) {
//        t << TreeNode::Depth::mDepth * 3 << " ";
//        return t.mTrace;
//    }
}
inline std::ostream& operator<<(std::ostream& os, BT::TreeNode::Depth const& d) {
    os << std::setw(d.mDepth * 3) << " ";
    return os;
}

