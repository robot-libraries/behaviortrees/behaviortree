//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//
//======================================================================================================================
#include <iomanip>
#include <string>

#include <BehaviorTree.h>

using namespace BT;

namespace BT {
    //--------------------------------------------------------------------------------------------------------------------------
    TreeNode::TreeNode(std::string const& name, TreeNode::NodeType const type, std::string const& node_name) :
        mName { name }, mType { type }, mNodeName { node_name } {
    }
    //--------------------------------------------------------------------------------------------------------------------------
    // Reset a node if it needs it to restart processing
    void TreeNode::reset() {
        mState = Failure;
    }
    //--------------------------------------------------------------------------------------------------------------------------
    std::string const TreeNode::getNodeText() const {
        std::string text = getNodeId() + " ";
        text += getName() + " ";
        text += getStateText(mState) + " ";
        return text;
    }
    //--------------------------------------------------------------------------------------------------------------------------
    std::string const& TreeNode::getName() const {
        return mName;
    }
    //--------------------------------------------------------------------------------------------------------------------------
    TreeNode::NodeState TreeNode::getState() const {
        return mState;
    }
    //--------------------------------------------------------------------------------------------------------------------------
    std::string const& TreeNode::getStateText(TreeNode::NodeState const ns) {
        static std::string const text[] = { "Failure ", "Success ", "Running " };
        return text[ns];
    }
    //--------------------------------------------------------------------------------------------------------------------------
    TreeNode::NodeType const TreeNode::getType() const {
        return mType;
    }
    //--------------------------------------------------------------------------------------------------------------------------
    std::string const& TreeNode::getTypeText(TreeNode::NodeType const nt) {
        static std::string const text[] = { "Action ", "Condition ", "Control " };
        return text[nt];
    }
    //--------------------------------------------------------------------------------------------------------------------------
    void TreeNode::setState(TreeNode::NodeState const new_state) {
        mState = new_state;
    }
    //--------------------------------------------------------------------------------------------------------------------------
    std::string TreeNode::getNodeId() const {
//        return typeid ( *this).name();
        return mNodeName;
    }
}
